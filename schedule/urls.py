from django.urls import re_path
from .views import index, newschedule, addschedule, resetschedule
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^addschedule', addschedule, name='addschedule'),
    re_path(r'^new_schedule', newschedule, name='newschedule'),
    re_path(r'^resetschedule', resetschedule, name='resetschedule'),
]
