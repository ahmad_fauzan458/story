from .models import Schedule, DAYS_CHOICES
from django.forms import ModelForm
from django import forms
import datetime

class ScheduleForm(ModelForm):

    title = forms.CharField(max_length=27, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': "Enter the title of the event ..."
    	}
    ))

    day = forms.ChoiceField(choices=DAYS_CHOICES, widget=forms.Select(
    	attrs={
    		'class': 'form-control',
    	}
    ))

    date = forms.DateField(widget=forms.DateInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'date',
    	}
    ))

    start_time = forms.TimeField(widget=forms.TimeInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'time',
    	}
    ))

    end_time = forms.TimeField(widget=forms.TimeInput(
    	attrs={
    		'class': 'form-control',
    		'type' : 'time',
    	}
    ))

    place = forms.CharField(max_length=27, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': "Enter the place of the event ..."
    	}
    ))

    category = forms.CharField(max_length=27, widget=forms.TextInput(
    	attrs={
    		'class': 'form-control',
    		'placeholder': "Enter the category of the event ..."
    	}
    ))

    class Meta:
        model = Schedule
        fields = '__all__'

    def clean_end_time(self):
        start_time = self.cleaned_data['start_time']
        end_time = self.cleaned_data['end_time']
        day = self.cleaned_data['day']
        try:
            date = self.cleaned_data['date']
        except :
            return start_time
       
        date_time_start = ('%s %s' % (date,start_time))
        date_time_start = datetime.datetime.strptime(date_time_start, '%Y-%m-%d %H:%M:%S')

        date_day = date_time_start.strftime("%A")
        if date_time_start < datetime.datetime.now():
            self.add_error('start_time', 'Cannot add past event')
        if day != date_day:
            self.add_error('day', "Day mismatched with date")
        if end_time < start_time:
            self.add_error('end_time', "End time should be greater than start time.")
        return start_time




