from django.db import models
from datetime import datetime, date


DAYS_CHOICES = [
	("Sunday", "Sunday"),
	("Monday", "Monday"),
	("Tuesday", "Tuesday"),
	("Wednesday", "Wednesday"),
	("Thursday", "Thursday"),
	("Friday", "Friday"),
	("Saturday", "Saturday"),
	]	

class Schedule(models.Model):
	title = models.CharField("Title", max_length=27)
	day = models.CharField("Day", choices=DAYS_CHOICES, max_length=9)
	date = models.DateField("Date")
	start_time = models.TimeField("Start Time")
	end_time = models.TimeField("End Time")
	place = models.CharField("Place", max_length=27) 
	category = models.CharField("Category", max_length=10)

