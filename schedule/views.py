from .forms import ScheduleForm
from django.shortcuts import render
from .models import Schedule
from django.http import HttpResponseRedirect

response = {}

def index(request):
	schedule = Schedule.objects.all().values().order_by('date', 'start_time')
	response['schedule'] = schedule

	return render(request, 'schedule.html', response)

def newschedule(request):
    """Create a new Schedule model."""
    form = ScheduleForm(request.POST or None)
    if request.method == 'POST' and form.is_valid():
    	response['title'] = request.POST['title']
    	response['day'] = request.POST['day']
    	response['date'] = request.POST['date']
    	response['start_time'] = request.POST['start_time']
    	response['end_time'] = request.POST['end_time']
    	response['place'] = request.POST['place']
    	response['category'] = request.POST['category']
    	schedule = Schedule(title=response['title'], day=response['day'], date=response['date'], 
			start_time=response['start_time'], end_time=response['end_time'], place=response['place'],
			category=response['category'])
    	schedule.save()
    	return HttpResponseRedirect("/../schedule/")
    response['ScheduleForm'] = form
    return render(request, "addschedule.html", response)

def addschedule(request):
	response['ScheduleForm'] = ScheduleForm
	return render(request, 'addschedule.html', response)

def resetschedule(request):
    schedule = Schedule.objects.all()
    for data in schedule:
        data.delete()
    return HttpResponseRedirect('/schedule')


